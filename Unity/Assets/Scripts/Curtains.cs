﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Curtains : MonoBehaviour {

    public Animator animator;
    public float closeTime;
    private static Curtains instance;

	void Start () {
        instance = this;
	}

    public static void CloseCurtains(UnityAction callback)
    {
        instance.animator.SetTrigger("Close");
        instance.StartCoroutine(instance.WaitForSecondAndExecute(instance.closeTime, callback));
    }

    public IEnumerator WaitForSecondAndExecute(float time, UnityAction callback)
    {
        Debug.Log("waiting " + time);
        yield return new WaitForSecondsRealtime(time);
        callback.Invoke();
    }

    public static void OpenCurtains()
    {
        instance.animator.SetTrigger("Open");
    }
}
