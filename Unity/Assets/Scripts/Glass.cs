﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Glass : MonoBehaviour
{
    public float breakThreshold = 5;
    public float breakTime;
    public int breakFrequnce;
    public int breakRange;
    public float value;
    public ParticleSystem ps;
    public bool setColor = true;
    public AudioSource[] breakingSounds;

    [HideInInspector]
    public Platform parent;

    public UnityEvent onBreak = new UnityEvent();

    private float breakTimer;
    private SpriteRenderer renderer;
    private PointEffector2D effector;

    private Animator animator;

    private void Awake()
    {
        renderer = GetComponent<SpriteRenderer>();
        breakFrequnce = Random.Range(breakRange + 1, 255);
        effector = GetComponent<PointEffector2D>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        bool inrange = false;
        if (parent.frequency > (breakFrequnce - breakRange) && parent.frequency < (breakFrequnce + breakRange))
        {
            inrange = true;
        }

        if (inrange)
        {
            breakTimer += Time.deltaTime;
            animator.SetBool("InRange", true);
            if (breakTimer >= breakTime)
            {
                breakTimer = 0;
                effector.enabled = true; if (breakingSounds.Length > 0)
                {
                    GameObject sound = (GameObject)GameObject.Instantiate(breakingSounds[Random.Range(0, breakingSounds.Length)].gameObject);
                    AudioSource source = sound.GetComponent<AudioSource>();
                    Destroy(sound, source.clip.length);
                }
                GameObject.Destroy(gameObject, .1f);
                if (ps != null)
                {
                    GameObject particle = GameObject.Instantiate(ps.gameObject);
                    particle.transform.position = transform.position;
                    GameObject.Destroy(particle, ps.main.duration);
                }
                Destroy();
            }
        }
        else
        {
            animator.SetBool("InRange", false);
            breakTimer = 0;
        }
        if (transform.position.y < -100)
        {
            GameObject.Destroy(gameObject);

            Destroy();
        }
    }

    public void Destroy()
    {
        onBreak.Invoke();
    }
    public void ReplaceRandomGlassWithPrefab(GameObject prefab)
    {
        GameManager.ReplaceRandomGlassWithPrefab(prefab);
    }

    public void RandomPitch()
    {
        GameManager.RandomPitch();
    }
    public void FreezePitch( float length)
    {
        GameManager.FreezePitch(length);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.relativeVelocity.magnitude > breakThreshold || collision.collider.gameObject.tag == "Ground")
        {
            effector.enabled = true;
            if (breakingSounds.Length > 0)
            {
                GameObject sound = (GameObject)GameObject.Instantiate(breakingSounds[Random.Range(0, breakingSounds.Length)].gameObject);
                AudioSource source = sound.GetComponent<AudioSource>();
                Destroy(sound, source.clip.length);
            }
            GameObject.Destroy(gameObject, .1f);
            if (ps != null)
            {
                GameObject particle = GameObject.Instantiate(ps.gameObject);
                particle.transform.position = transform.position;
                GameObject.Destroy(particle, ps.main.duration);
            }

            Destroy();
        }
    }
}
