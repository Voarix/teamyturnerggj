﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSceneOnAwake : MonoBehaviour
{

    public string name;

    void Start()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(name);
    }
}
