﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    static GameManager instance;

    public Platform[] platforms;
    public Glass[] powerUps;
    public EndScreen endScreen;


    public int p1Score;
    public int p2Score;

    private GameObject scoreP11, scoreP12, scoreP21, scoreP22;

    private void Awake()
    {
        DOTween.Init();
        instance = this;
        if (FindObjectsOfType<GameManager>().Length > 1)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
            FinishGame();
        if (endScreen == null)
        {
            instance.endScreen = FindObjectOfType<EndScreen>();
            if (instance.endScreen != null)
            {
                instance.endScreen.gameObject.SetActive(false);
            }

        }
        if (instance.scoreP11 == null)
        {
            instance.scoreP11 = GameObject.Find("scoreP11");
            instance.scoreP12 = GameObject.Find("scoreP12");
            instance.scoreP21 = GameObject.Find("scoreP21");
            instance.scoreP22 = GameObject.Find("scoreP22");

            if (instance.scoreP11 != null)
            {
                instance.scoreP11.SetActive(instance.p1Score >= 1);
                instance.scoreP12.SetActive(instance.p1Score >= 2);
                instance.scoreP21.SetActive(instance.p2Score >= 1);
                instance.scoreP22.SetActive(instance.p2Score >= 2);
            }
        }
    }

    public static void SpawnPowerUp(Platform self, Vector3 start)
    {
        instance.platforms = FindObjectsOfType<Platform>();
        foreach (var item in instance.platforms)
        {
            if (item != self)
            {
                if (instance.powerUps.Length > 0)
                    item.SpawnPowerUp(instance.powerUps[Random.Range(0, instance.powerUps.Length)], start);
            }
        }
    }

    public static void ReplaceRandomGlassWithPrefab(GameObject prefab)
    {
        var platform = instance.platforms[Random.Range(0, instance.platforms.Length)];
        platform.ReplaceRandomGlassWithPrefab(prefab);

    }

    public static void FinishGame(bool checkScores = true)
    {
        instance.endScreen.gameObject.SetActive(true);
        instance.endScreen.StartCountdown(5, checkScores);
        foreach (var item in instance.platforms)
        {
            if (item != null)
            {
                foreach (var glass in item.glasses)
                {
                    Score s = glass.GetComponent<Score>();
                    if (s != null)
                        item.targetScore += s.value;
                }
            }
        }

        instance.platforms = FindObjectsOfType<Platform>();
    }

    public static void CheckScores(bool updateScores = true)
    {
        AudioSource[] sources = FindObjectsOfType<AudioSource>();
        foreach (var item in sources)
        {
            if (item.tag != "Background")
                item.enabled = false;
        }
        if (updateScores)
        {
            if (instance.platforms[0].targetScore > instance.platforms[1].targetScore)
            {
                instance.p1Score++;

                if (instance.scoreP11 != null)
                {
                    instance.scoreP11.SetActive(instance.p1Score >= 1);
                    instance.scoreP12.SetActive(instance.p1Score >= 2);
                    instance.scoreP21.SetActive(instance.p2Score >= 1);
                    instance.scoreP22.SetActive(instance.p2Score >= 2);
                }

                if (instance.p1Score == 1)
                {
                    instance.scoreP11.SetActive(true);
                }
                if (instance.p1Score == 2)
                {
                    GameObject.Find("scoreP12").SetActive(true);
                }

                instance.platforms[0].animator.SetTrigger("scoreTriggerWinner");
            }
            else
            {
                instance.p2Score++;

                instance.scoreP11.SetActive(instance.p1Score >= 1);
                instance.scoreP12.SetActive(instance.p1Score >= 2);
                instance.scoreP21.SetActive(instance.p2Score >= 1);
                instance.scoreP22.SetActive(instance.p2Score >= 2);


                if (instance.p2Score == 1)
                {
                    GameObject.Find("scoreP21").SetActive(true);
                }
                if (instance.p2Score == 2)
                {
                    GameObject.Find("scoreP22").SetActive(true);
                }

                instance.platforms[1].animator.SetTrigger("scoreTriggerWinner");
            }
        }

        if (instance.p1Score == 2 || instance.p2Score == 2)
        {
            if (instance.p1Score == 2)
            {
                instance.platforms[0].WonGame();
            }
            else if (instance.p2Score == 2)
            {
                instance.platforms[1].WonGame();
            }

            instance.p1Score = 0;
            instance.p2Score = 0;
            instance.endScreen.retryButton.onClick.RemoveAllListeners();
            instance.endScreen.retryButton.onClick.AddListener(() =>
            {
                Curtains.CloseCurtains(() =>
                {
                    Time.timeScale = 1;
                    UnityEngine.SceneManagement.SceneManager.LoadScene(0);
                });
            });
        }
        else
        {
            instance.endScreen.retryButton.onClick.RemoveAllListeners();
            instance.endScreen.retryButton.onClick.AddListener(() =>
            {
                Curtains.CloseCurtains(() =>
                {
                    Time.timeScale = 1;
                    UnityEngine.SceneManagement.SceneManager.LoadScene(1);
                });
            });
        }
    }

    public static void RandomPitch()
    {
        var sliders = FindObjectsOfType<FrequencySlider>();
        var slider = sliders[Random.Range(0, sliders.Length)];
        slider.slider.value = Random.Range(slider.slider.minValue, slider.slider.maxValue);
    }

    public static void FreezePitch(float length)
    {
        var sliders = FindObjectsOfType<FrequencySlider>();
        var slider = sliders[Random.Range(0, sliders.Length)];
        slider.FreezePitch(length);
    }
}
