﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour {

    public Text countdownText;
    public Button retryButton;

    public void StartCountdown(float time, bool checkScores = true)
    {
        StartCoroutine(Countdown(time, ()=>
        {
            GameManager.CheckScores(checkScores);
            if (retryButton != null)
                retryButton.gameObject.SetActive(true);
            Time.timeScale = 0;
        }));
    }

    IEnumerator Countdown(float time, UnityAction onComplete)
    {
        while(time != 0)
        {
            if (countdownText)
                countdownText.text = time.ToString();
            time--;
            yield return new WaitForSeconds(1);
        }
        countdownText.gameObject.SetActive(false);
        onComplete.Invoke();
    }

    public void LoadLevel(int id)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(id);
    }

    public void SetTimeScale(float timescale)
    {
        Time.timeScale = timescale;
    }


}
