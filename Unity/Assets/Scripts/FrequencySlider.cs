﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Audio;

public class FrequencySlider : MonoBehaviour
{
    public Platform target;
    public float frequency;
    public KeyCode increaseButton;
    public KeyCode decreaseButton;
    public float speed = 2;
    public float minPitch, maxPitch;
    public AudioMixer mixer;
    public string pitchName, volumeName;
    public float stopThreshold = 1;
    public float highVolume;
    public float normalVolume;
    public float volumeSpeed = 5;
    [HideInInspector]
    public Slider slider;
    private float targetVolume;
    private bool frozen;

    private void Awake()
    {
        slider = GetComponent<Slider>();
        slider.maxValue = 255;
        slider.minValue = 0;
        if(mixer != null)
        {
            mixer.GetFloat(volumeName, out targetVolume);
        }
    }

    public void FreezePitch(float length)
    {
        StartCoroutine(Freeze(length));
    }

    IEnumerator Freeze(float length)
    {
        frozen = true;
        yield return new WaitForSeconds(length);

        frozen = false;
    }

    void Update()
    {
        if (!frozen)
        {
            if (Mathf.Abs(slider.value - frequency) < 1)
            {
                if (mixer != null)
                {
                    targetVolume = highVolume;
                }
            }
            else
            {
                if (mixer != null)
                {
                    targetVolume = normalVolume;
                }
            }

            target.frequency = slider.value;
            if (Input.GetKey(increaseButton))
            {
                slider.value += speed * Time.deltaTime;
            }
            if (Input.GetKey(decreaseButton))
            {
                slider.value -= speed * Time.deltaTime;
            }
            if (mixer != null)
            {
                mixer.SetFloat(pitchName, Utils.Remap(frequency, 0, 255, minPitch, maxPitch));
                float volume = 0;
                mixer.GetFloat(volumeName, out volume);
                volume = Mathf.MoveTowards(volume, targetVolume, Time.deltaTime * volumeSpeed);
                mixer.SetFloat(volumeName, volume);
            }
        }
    }
}
