﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SpawnerWindow : EditorWindow
{

    public int baseCount = 20;
    public GameObject prefab;
    public GameObject parent;

    [MenuItem("Window/Glass Spawner")]
    public static void Show()
    {
        EditorWindow.GetWindow<SpawnerWindow>();
    }

    public void SpawnGlasses()
    {
        float xOffset = 0;
        int layer = 1;
        baseCount = Mathf.Abs(baseCount);
        while (baseCount != 0)
        {
            for (int i = 0; i < baseCount; i++)
            {
                GameObject go = (GameObject)PrefabUtility.InstantiatePrefab(prefab );
                go.transform.position = new Vector3(i + xOffset, layer, 0);
                go.transform.SetParent(parent.transform);
            }
            layer++;
            xOffset += .32f;
            baseCount--;
        }
    }

    private void OnGUI()
    {
        baseCount = EditorGUILayout.IntField("Base Count", baseCount);
        prefab = (GameObject)EditorGUILayout.ObjectField("Prefab", prefab, typeof(GameObject), false);
        parent = (GameObject)EditorGUILayout.ObjectField("parent", parent, typeof(GameObject), true);
        if(GUILayout.Button("Spawn"))
        {
            SpawnGlasses();
        }

    }
}
