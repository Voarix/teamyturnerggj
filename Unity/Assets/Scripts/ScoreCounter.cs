﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour
{

    public Text leftText;
    public Text rightText;
    private Score[] glasses;

    public float center;

    private void Awake()
    {
        glasses = GameObject.FindObjectsOfType<Score>();
    }

    void Update()
    {
        float left = 0;
        float right = 0;
        foreach (var item in glasses)
        {
            if (item != null)
            {
                if (item.transform.position.x > center)
                    right += item.value;
                else
                    left += item.value;
            }
        }
        leftText.text = left.ToString();
        rightText.text = right.ToString();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawLine(new Vector3(center, 100, 0), new Vector3(center, -100, 0));
    }
}
