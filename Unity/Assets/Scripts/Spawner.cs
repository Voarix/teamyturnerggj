﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    public GameObject prefab;
    public int baseCount;

	void Start () {
        float xOffset = 0;
        int layer = 1;
        Camera.main.transform.position = new Vector3(baseCount / 2, baseCount / 2, -10);
        Camera.main.orthographicSize = baseCount / 2 + 1;
	    while(baseCount != 0)
        {
            for (int i = 0; i < baseCount; i++)
            {
                GameObject go = GameObject.Instantiate(prefab);
                go.transform.position = new Vector3(i + xOffset, layer, 0);
            }
            layer++;
            xOffset += .5f;
            baseCount--;
        }

	}
}
