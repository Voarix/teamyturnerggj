﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomQuote : MonoBehaviour {

    public List<Sprite> quotes;

	// Use this for initialization
	void Start () {
        this.GetComponent<SpriteRenderer>().sprite = quotes[Random.Range(0, quotes.Count)];
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
