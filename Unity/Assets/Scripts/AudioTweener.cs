﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class AudioTweener : MonoBehaviour
{
    public AudioSource[] sources;
    public Slider slider;
    private int currentIndex;
    private float prevValue;

    private void Awake()
    {
        prevValue = slider.value;
        for (int i = 0; i < sources.Length; i++)
        {
            if (i != 0)
                sources[i].volume = 0;
        }
    }

    void Update()
    {
        if (currentIndex > 0)
        {
            float val = slider.maxValue / (sources.Length - 1) * (currentIndex - 1);
            if (prevValue > val && slider.value <= val)
            {
                sources[currentIndex].DOFade(0, 1f);
                sources[currentIndex - 1].DOFade(1, 1f);
                currentIndex--;
            }
        }

        if (currentIndex < sources.Length - 1)
        {
            float val = slider.maxValue / (sources.Length - 1) * (currentIndex + 1);
            if (prevValue < val && slider.value >= val)
            {
                sources[currentIndex].DOFade(0, 1f);
                sources[currentIndex + 1].DOFade(1, 1f);
                currentIndex++;
            }
        }


        prevValue = slider.value;
    }
}
