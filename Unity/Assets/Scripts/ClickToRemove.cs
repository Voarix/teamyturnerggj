﻿using UnityEngine;
using System.Collections;

public class ClickToRemove : MonoBehaviour {

    private void Awake()
    {
        GetComponent<SpriteRenderer>().color = Random.ColorHSV();
    }

    private void OnMouseDown()
    {
        Destroy(gameObject);
    }
}
