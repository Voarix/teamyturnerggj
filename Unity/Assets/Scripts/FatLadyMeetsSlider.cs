﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class FatLadyMeetsSlider : MonoBehaviour {

    public GameObject head, armLeftUpper, armLeftLower;

    public Vector2 minMaxHeadTilt;
    public Vector2 minMaxArmLeftUpper;
    public Vector2 minMaxArmLeftLower;
    public Color headBeginColor;
    public Color headEndColor;

    public Slider pitchSlider;

    public float normalizedSliderPos;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        normalizedSliderPos = pitchSlider.value / pitchSlider.maxValue;

        Vector3 newHeadAngle = new Vector3(0, 0, Mathf.LerpAngle(minMaxHeadTilt.x, minMaxHeadTilt.y, normalizedSliderPos));
        Vector3 newLeftArmAngle = new Vector3(0, 0, Mathf.LerpAngle(minMaxArmLeftUpper.x, minMaxArmLeftUpper.y, normalizedSliderPos));
        Vector3 newLeftArmLowerAngle = new Vector3(0, 0, Mathf.LerpAngle(minMaxArmLeftLower.x, minMaxArmLeftLower.y, normalizedSliderPos));
        Color lerpedColor = Color.Lerp(headBeginColor, headEndColor, normalizedSliderPos);

        head.transform.eulerAngles = newHeadAngle;
        armLeftUpper.transform.eulerAngles = newLeftArmAngle;
        armLeftLower.transform.localEulerAngles = newLeftArmLowerAngle;
        head.GetComponent<SpriteRenderer>().color = lerpedColor;
    }
}
