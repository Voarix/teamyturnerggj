﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectClusters : MonoBehaviour
{

    public List<GameObject> remainingGlasses;

    public float clusterRange = 2f;

    public GameObject selectedGlass;

    int colorCounter;

    // Use this for initialization
    void Start()
    {
        foreach (Transform childGlass in transform)
        {
            remainingGlasses.Add(childGlass.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            selectedGlass = remainingGlasses[Random.Range(0, remainingGlasses.Count)];
            selectedGlass.GetComponentInChildren<SpriteRenderer>().color = Color.green;

            Color randomColor = Random.ColorHSV(0.0f, 1.0f, 0.8f, 1.0f);



            if (clusterRange != 0f)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(new Vector2(selectedGlass.transform.position.x, selectedGlass.transform.position.y), clusterRange);

                foreach (Collider2D col in colliders)
                {
                    col.gameObject.GetComponentInChildren<SpriteRenderer>().color = Color.blue;
                }
            }
            colorCounter++;
            if (colorCounter > 3)
            {
                foreach (GameObject childGlass in remainingGlasses)
                {
                    if (childGlass != null)
                        childGlass.gameObject.GetComponentInChildren<SpriteRenderer>().color = Color.white;
                }
                colorCounter = 0;
            }
        }
    }
}
