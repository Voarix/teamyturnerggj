﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDoneChecker : MonoBehaviour
{
    Platform[] platforms;

    void Start()
    {
        platforms = FindObjectsOfType<Platform>();
    }

    void Update()
    {
        if (platforms != null)
        {
            bool anyRemaining = false;
            foreach (var item in platforms)
            {
                if (item.glasses.Count > 0)
                    anyRemaining = true;
            }
            if(!anyRemaining)
            {
                AudioSource[] sources = FindObjectsOfType<AudioSource>();
                foreach (var item in sources)
                {
                    if (item.tag != "Background")
                        item.enabled = false;
                }
                Curtains.CloseCurtains(() =>
                {
                    Time.timeScale = 1;
                    UnityEngine.SceneManagement.SceneManager.LoadScene(1);
                });
                Destroy(this);
            }
        }
    }
}
