﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using UnityEngine.Events;

public class Platform : MonoBehaviour
{
    public float frequency;
    public float scoreSpeed = 100;
    public Text scoreText;
    public int score;
    public int targetScore;
    public Animator animator;
    public List<Glass> glasses = new List<Glass>();
    public List<Glass> targets = new List<Glass>();
    public GameObject glassPrefab;
    public UnityEvent OnWin;

    void Start()
    {
        glasses = GetComponentsInChildren<Glass>().ToList();
        foreach (var item in glasses)
        {
            item.parent = this;
            if (item.tag != "TargetGlass")
            {
                Score s = item.GetComponent<Score>();
                item.onBreak.AddListener(() =>
                {
                    UpdateScore(score);
                    glasses.Remove(item);
                });
            }
            else
            {
                targets.Add(item);
                item.onBreak.AddListener(() =>
                {
                    Score s = item.GetComponent<Score>();
                    if (s != null)
                    {
                        targetScore += s.value;
                        if(animator != null)
                        {
                            animator.SetTrigger("ScoreIncreased");
                        }
                    }

                    targets.Remove(item);
                    GameManager.SpawnPowerUp(this, item.transform.position);
                    if (targets.Count == 0)
                    {
                        GameManager.FinishGame();
                    }
                });
            }
        }

        foreach (var item in targets)
        {
            glasses.Remove(item);
        }
        UpdateScore(score);
    }

    public void WonGame()
    {
        OnWin.Invoke();
    }

    private void Update()
    {
        score = (int)Mathf.MoveTowards(score, targetScore, Time.unscaledDeltaTime * scoreSpeed);
        UpdateScore(score);
    }

    public void ReplaceRandomGlassWithPrefab(GameObject prefab)
    {
        if (glasses.Count > 0)
        {
            Glass glass = glasses[UnityEngine.Random.Range(0, glasses.Count)];
            glass.Destroy();
            GameObject go = Instantiate(prefab);
            go.transform.position = glass.transform.position;
            Destroy(glass.gameObject);
            glass = go.GetComponent<Glass>();
            List<Glass> childGlasses = new List<Glass>();
            childGlasses.AddRange(go.GetComponentsInChildren<Glass>().ToList());
            if (glass != null)
                childGlasses.Add(glass);
            foreach (var item in childGlasses)
            {
                item.parent = this;
            }
            UpdateScore(score);
        }
    }

    public void UpdateScore(float score)
    {
        if (scoreText != null)
            scoreText.text = score.ToString();
    }

    public void SpawnPowerUp(Glass prefab, Vector3 startPos)
    {
        Glass glass = glasses[UnityEngine.Random.Range(0, glasses.Count)];
        glass.Destroy();
        GameObject go = Instantiate(prefab.gameObject);
        go.transform.position = glass.transform.position;

        GameObject go2 =GameObject.Instantiate(glassPrefab);
        go2.transform.position = glass.transform.position;
        go2.GetComponent<Glass>().parent = this;
        
        Destroy(glass.gameObject);
        glass = go.GetComponent<Glass>();
        glass.parent = this;
        glass.Destroy();
        Destroy(glass.gameObject);

    }
}
